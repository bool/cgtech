<vertex_program_start>

#version 330 core

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;

uniform mat4 matrixModelViewProjection;
uniform mat4 matrixModel;

varying vec3 n;

void main(void)
{
	gl_Position = matrixModelViewProjection * vec4(vertex, 1);
	n = transpose(mat3(inverse(matrixModel))) * normal;
}

<vertex_program_end>

<fragment_program_start>

#version 330 core

varying vec3 n;

uniform vec4 color;
uniform vec4 lightCol;
uniform vec3 lightDir;

void main(void)
{
	//normalize input variables
	vec3 N = normalize(n);
	vec3 L = normalize(lightDir);
	
	float NdotL = max(0, min(1, dot(N, L)));
	vec3 n2 = abs(normalize(N*0.5 + 0.5));

	vec4 outputColor = color;
	outputColor.xyz *= NdotL * lightCol.xyz;
	
	gl_FragColor = outputColor;
}

<fragment_program_end>
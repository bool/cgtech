/*
 * shader_program.h
 *
 *  Created on: 19.09.2015
 *      Author: vasiliy
 */

#ifndef CGTECHNIQUESTEPS_SOURCES_SHADERS_SHADER_PROGRAM_H_
#define CGTECHNIQUESTEPS_SOURCES_SHADERS_SHADER_PROGRAM_H_

#include <string>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

/*
 * shader contract description
 *
 * <vertex_program_start>
 *  vertex shader code...
 * <vertex_program_end>
 *
 * <fragment_program_start>
 *  fragment shader code...
 * <fragment_program_end>
 */

#ifndef BUFFER_OFFSET
#define BUFFER_OFFSET(offset) ((void*)(0 + offset))
#endif

#define VERT_PROGRAM_TAG_START "<vertex_program_start>"
#define VERT_PROGRAM_TAG_END "<vertex_program_end>"
#define FRAG_PROGRAM_TAG_START "<fragment_program_start>"
#define FRAG_PROGRAM_TAG_END "<fragment_program_end>"

class CShaderProgram
{
public:
	//fields
	GLuint shader_program_id;
	GLuint vertex_shader_id;
	GLuint fragment_shader_id;

	//methods
	std::string parse_content_from_tags(const std::string& content, const std::string& start, const std::string& end) const;
	std::string parse_vertex_program(const std::string& content);
	std::string parse_fragment_program(const std::string& content);
	GLuint create_shader(GLenum shaderType, const std::string& shader_source_code);
public:
	CShaderProgram();
	virtual ~CShaderProgram();

	bool load(const std::string& fileName);
	void bind();
	void free();

	//uniforms
	void setUniformVector(const std::string& uniform, const glm::vec4& v) const;
	void setUniformVector(const std::string& uniform, const glm::vec3& v) const;
	void setUniformMatrix(const std::string& uniform, const glm::mat4& matrix) const;

	//vertex attributes
	GLint getAttribLocation(const GLchar* attribute) const;
};

#endif /* CGTECHNIQUESTEPS_SOURCES_SHADERS_SHADER_PROGRAM_H_ */

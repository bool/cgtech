/*
 * shader_program.cpp
 *
 *  Created on: 20.09.2015
 *      Author: vasiliy
 */

#include "shader_program.h"

/*
 * shader_program.cpp
 *
 *  Created on: 13.09.2015
 *      Author: vasiliy
 */

#include "shader_program.h"
#include <iostream>
#include <fstream>

using namespace std;

void CShaderProgram::setUniformVector(const std::string& uniform,
		const glm::vec4& v) const
{
	int location = glGetUniformLocation(shader_program_id, uniform.c_str());
	if (location == -1)
	{
		cout << "can't set uniform by name '" << uniform << "'" << endl;
		return;
	}

	glUniform4fv(location, 1, glm::value_ptr(v));
}

void CShaderProgram::setUniformVector(const std::string& uniform,
		const glm::vec3& v) const
{
	int location = glGetUniformLocation(shader_program_id, uniform.c_str());
	if (location == -1)
	{
		cout << "can't set uniform by name '" << uniform << "'" << endl;
		return;
	}

	glUniform3fv(location, 1, glm::value_ptr(v));
}

void CShaderProgram::setUniformMatrix(const std::string& uniform,
		const glm::mat4& matrix) const
{
	int location = glGetUniformLocation(shader_program_id, uniform.c_str());
	if (location == -1)
	{
		cout << "can't set uniform by name '" << uniform << "'" << endl;
		return;
	}

	glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
}

GLint CShaderProgram::getAttribLocation(const GLchar* attribute) const
{
	GLint location = glGetAttribLocation(shader_program_id, attribute);
	if (location == -1)
	{
		cout << "not found vertex attribute by name '" << attribute << "'"
				<< endl;
	}
	else
	{
		//cout << "attribute location by name '" << attribute << "' = " << location << endl;
	}

	return location;
}

CShaderProgram::CShaderProgram() :
		shader_program_id(0), vertex_shader_id(0), fragment_shader_id(0)
{
	int maxAttribs;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &maxAttribs);
	cout << "GL max vertex attribs = " << maxAttribs << endl;
}

CShaderProgram::~CShaderProgram()
{
}

std::string CShaderProgram::parse_content_from_tags(const std::string& content,
		const std::string& start, const std::string& end) const
{
	string output("");
	size_t nStartPos = content.find(start, 0) + start.size() + 1;
	if (nStartPos < content.size())
	{
		size_t nEndPos = content.find(end, nStartPos);
		size_t length = nEndPos - nStartPos;
		if (length > 0)
		{
			output.reserve(length);
			output.assign(content.substr(nStartPos, length));
		}
	}
	return output;
}

std::string CShaderProgram::parse_vertex_program(const std::string& content)
{
	std::string output(
			parse_content_from_tags(content, VERT_PROGRAM_TAG_START,
					VERT_PROGRAM_TAG_END));
	cout << "vertex shader:" << endl << output << "...the end..." << endl;
	return output;
}

std::string CShaderProgram::parse_fragment_program(const std::string& content)
{
	std::string output(
			parse_content_from_tags(content, FRAG_PROGRAM_TAG_START,
					FRAG_PROGRAM_TAG_END));
	cout << "fragment shader:" << endl << output << "...the end..." << endl;
	return output;
}

bool CShaderProgram::load(const std::string& fileName)
{
	ifstream file;
	file.open(fileName.c_str(), ios::binary);

	if (!file.is_open())
	{
		cout << "can't open shader file '" << fileName << "'" << endl;
		return false;
	}

	string file_content;
	file.seekg(0, ios::end);
	file_content.reserve(file.tellg());
	file.seekg(0, ios::beg);
	file_content.assign(istreambuf_iterator<char>(file),
			istreambuf_iterator<char>());

	auto vertProgramSourceCode = parse_vertex_program(file_content);
	auto fragProgramSourceCode = parse_fragment_program(file_content);

	//gl create program
	vertex_shader_id = create_shader(GL_VERTEX_SHADER, vertProgramSourceCode);
	fragment_shader_id = create_shader(GL_FRAGMENT_SHADER,
			fragProgramSourceCode);
	shader_program_id = glCreateProgram();
	glAttachShader(shader_program_id, vertex_shader_id);
	glAttachShader(shader_program_id, fragment_shader_id);
	glLinkProgram(shader_program_id);

	GLint linked;
	glGetProgramiv(shader_program_id, GL_LINK_STATUS, &linked);
	if (!linked)
	{
		return false;
	}

	return true;
}

GLuint CShaderProgram::create_shader(GLenum shaderType,
		const std::string& shader_source_code)
{
	int shader_id = glCreateShader(shaderType);
	if (shader_id == -1)
	{
		cout << "can't create shader from type '" << shaderType << "'" << endl;
		return -1;
	}

	GLsizei length = shader_source_code.size();
	const char *source = shader_source_code.c_str();
	glShaderSource(shader_id, 1, (const GLchar**) &source, &length);
	glCompileShader(shader_id);

	GLint compiled;
	glGetShaderiv(shader_id, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		GLsizei log_length;
		glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &log_length);
		if (log_length != 0)
		{
			char *buf = new char[log_length];
			glGetShaderInfoLog(shader_id, log_length, &log_length, buf);
			cout << "compile shader log:" << endl << buf << endl;
			delete[] buf;
		}
	}

	return shader_id;
}

void CShaderProgram::bind()
{
	glUseProgram(shader_program_id);
}

void CShaderProgram::free()
{
	glDeleteShader(vertex_shader_id);
	glDeleteShader(fragment_shader_id);
}

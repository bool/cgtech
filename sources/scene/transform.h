/*
 * transform.h
 *
 *  Created on: 09.11.2015
 *      Author: vasiliy
 */

#ifndef SCENE_TRANSFORM_H_
#define SCENE_TRANSFORM_H_

#include "../pch.h"

//predefinition class
class Transform;

//transform class definition
class Transform
{
protected:

	//transform name in scene
	String m_name;

	//transform scene tree nodes
	std::list<Transform*> m_childrenTransforms;

	//parent transform
	Transform *m_parent;

public:

	glm::vec3 m_position; //position in world space
	glm::quat m_rotation; //rotation in world space (quaternion)
	glm::vec3 m_scale;    //scale

public:

	Transform(const std::string& _name = "transform");
	virtual ~Transform() {}

	const std::list<Transform*>& getAllChildren();
	const glm::mat4 getMatrix() const;

	void attach(Transform* child);
	void detach(Transform* child);

	//helpers
	static glm::mat4 calculateMatrixTRS(const glm::vec3& translate, const glm::quat& rotation, const glm::vec3& scale);
};

#endif /* SCENE_TRANSFORM_H_ */

/*
 * sceneTree.cpp
 *
 *  Created on: 10.11.2015
 *      Author: vasiliy
 */

//precompiled header
#include "../pch.h"

#include "sceneTree.h"
#include "transform.h"
#include "light.h"

using namespace std;

SceneTree::SceneTree()
{
	m_root = new Transform("root");
}

SceneTree::~SceneTree()
{
	delete m_root;
}

const Transform* SceneTree::getRoot() const
{
	return m_root;
}

/**
 * add transform to scene graph and parenting to root by default
 */
void SceneTree::createTransform(Transform* _transform)
{
	if (_transform == nullptr)
		return;

	//check contains in m_transforms ?
	auto it = find(m_transforms.begin(), m_transforms.end(), _transform);
	if (it != m_transforms.end())
		return;

	//add to transforms list and parenting to m_root by default
	m_transforms.push_back(_transform);
	m_root->attach(_transform);
}

/**
 * remove transform from scene graph and kill links in all childrens
 */
void SceneTree::removeTransform(Transform* _transform)
{
}

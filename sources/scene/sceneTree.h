/*
 * sceneTree.h
 *
 *  Created on: 10.11.2015
 *      Author: vasiliy
 */

#ifndef SCENE_SCENETREE_H_
#define SCENE_SCENETREE_H_

class Transform;
class Light;

class SceneTree
{
protected:
	/**
	 * hello Unity3D!
	 */
	Transform *m_root;

	/**
	 * all transforms list
	 */
	std::list<Transform*> m_transforms;

	/**
	 * all lights for calculate light-space frustums
	 */
	std::list<Light*> m_lights;
public:
	SceneTree();
	virtual ~SceneTree();

	const Transform* getRoot() const;

	/**
	 * add transform to scene graph and parenting to root by default
	 */
	void createTransform(Transform* _transform);

	/**
	 * remove transform from scene graph and kill links in all childrens
	 */
	void removeTransform(Transform* _transform);

	void draw();
};

#endif /* SCENE_SCENETREE_H_ */

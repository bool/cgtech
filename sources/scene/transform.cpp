/*
 * transform.cpp
 *
 *  Created on: 10.11.2015
 *      Author: vasiliy
 */

#include "transform.h"
#include <algorithm>

using namespace glm;
using namespace std;

Transform::Transform(const std::string& _name)
//initalize all class fields
	: m_name(_name)
	, m_parent(0)
	, m_position(0.0f, 0.0f, 0.0f)
	, m_rotation(vec3(0.0f, 0.0f, 0.0f))
	, m_scale(1.0f, 1.0f, 1.0f)
{
}

void Transform::attach(Transform* child)
{
	if (child == NULL) {
		return;
	}

	auto it = std::find(m_childrenTransforms.begin(), m_childrenTransforms.end(), child);
	if (it != m_childrenTransforms.end()) {
		return;
	}

	m_childrenTransforms.push_back(child);
	child->m_parent = this;
}

void Transform::detach(Transform* child)
{
	if (child == NULL) {
		return;
	}

	auto it = std::find(m_childrenTransforms.begin(), m_childrenTransforms.end(), child);
	if (it == m_childrenTransforms.end()) {
		return;
	}

	m_childrenTransforms.erase(it);
	child->m_parent = NULL;
}

glm::mat4 Transform::calculateMatrixTRS(const glm::vec3& t, const glm::quat& r, const glm::vec3& s)
{
	glm::mat4 matrix;
	matrix *= translate(t);
	matrix *= toMat4(r);
	matrix *= scale(s);
	return matrix;
}

const glm::mat4 Transform::getMatrix() const
{
	//calculate self matrix TRS
	glm::mat4 matrix = calculateMatrixTRS(m_position, m_rotation, m_scale);
	if (m_parent) {
		matrix = m_parent->getMatrix() * matrix;
	}
	return matrix;
}

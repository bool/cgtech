/*
 * light.h
 *
 *  Created on: 10.11.2015
 *      Author: vasiliy
 */

#ifndef SCENE_LIGHT_H_
#define SCENE_LIGHT_H_

class Transform;
class Light
{
protected:
	Transform* m_transform;
public:
	enum class LightType
	{
		directional,
		spot,
		point
	};
};

#endif /* SCENE_LIGHT_H_ */

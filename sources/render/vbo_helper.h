/*
 * vbo_helper.h
 *
 *  Created on: 04.11.2015
 *      Author: vasiliy
 */

#ifndef CGTECHNIQUESTEPS_SOURCES_RENDER_VBO_HELPER_H_
#define CGTECHNIQUESTEPS_SOURCES_RENDER_VBO_HELPER_H_

#include <GL/glew.h>

//вершинные аттрибуты:
//позиция
#define VERT_ATTRIB_POSITION  	0
//нормаль
#define VERT_ATTRIB_NORMAL		1
//текстурная координата
#define VERT_ATTRIB_TEXCOORD0 	2

#endif /* CGTECHNIQUESTEPS_SOURCES_RENDER_VBO_HELPER_H_ */

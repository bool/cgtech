/*
 * material.h
 *
 *  Created on: 02.11.2015
 *      Author: vasiliy
 */

#ifndef CGTECHNIQUESTEPS_SOURCES_RENDER_MATERIAL_H_
#define CGTECHNIQUESTEPS_SOURCES_RENDER_MATERIAL_H_

#include <string>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

enum MaterialConstantType
{
	TYPE_FLOAT,
	TYPE_INT,
	TYPE_VEC2,
	TYPE_VEC3,
	TYPE_VEC4,
	TYPE_MAT2,
	TYPE_MAT3,
	TYPE_MAT4,
	TYPE_SAMPLER_2D,
	TYPE_SAMPLER_3D,
	TYPE_SAMPLER_CUBE,
};

struct MaterialConstant
{
	std::string name;
	MaterialConstantType type;

	//fields in memory
	float 		floatValue;
	int 		intValue;
	glm::vec1 	vec1Value;
	glm::vec2 	vec2Value;
	glm::vec3 	vec3Value;
	glm::vec4 	vec4Value;
};

struct Material
{
	std::string name;
	float shininess;
	glm::vec4 color;
};

#endif /* CGTECHNIQUESTEPS_SOURCES_RENDER_MATERIAL_H_ */

/*
 * string_utils.cpp
 *
 *  Created on: 17.09.2015
 *      Author: vasiliy
 */

#include "string_utils.h"
#include <iostream>
#include <vector>
#include <sstream>

namespace string_utils
{
	std::string string_trim_start(const std::string& str, const char character)
	{
		std::string s(str);
		s.erase(0, s.find_first_not_of(character));
		return s;
	}

	std::string string_trim_end(const std::string& str, const char character)
	{
		std::string s(str);
		s.erase(s.find_last_not_of(character) + 1);
		return s;
	}

	std::string string_trim(const std::string& str, const char character)
	{
		std::string s(str);
		s.erase(0, s.find_first_not_of(character));
		s.erase(s.find_last_not_of(character) + 1);
		return s;
	}

	std::string get_file_extension(const std::string& path)
	{
		return path.substr(path.find_last_of('.') + 1);
	}

	std::vector<std::string> string_split(const std::string& str, const char character, bool ignore_empty)
	{
		std::vector<std::string> output;
		std::stringstream ss(str);
		std::string token;
		while (std::getline(ss, token, character)) {
			token = string_trim(token, ' ');
			token = string_trim(token, character);
			if (!token.empty() || (token.empty() && !ignore_empty)) {
				output.push_back(token);
			}
		}
		return output;
	}
}

/*
 * string_utils.h
 *
 *  Created on: 17.09.2015
 *      Author: vasiliy
 */

#ifndef CGTECHNIQUESTEPS_SOURCES_UTILS_STRING_UTILS_H_
#define CGTECHNIQUESTEPS_SOURCES_UTILS_STRING_UTILS_H_

#include <string>
#include <algorithm>

namespace string_utils
{
	std::string get_file_extension(const std::string& path);

	std::string string_trim_start(const std::string& str, const char character);
	std::string string_trim_end(const std::string& str, const char character);
	std::string string_trim(const std::string& str, const char character);
	std::vector<std::string> string_split(const std::string& str, const char character, bool ignore_empty = true);
}

#endif /* CGTECHNIQUESTEPS_SOURCES_UTILS_STRING_UTILS_H_ */

/*
 * mesh.h
 *
 *  Created on: 05.11.2015
 *      Author: vasiliy
 */

#ifndef CGTECHNIQUESTEPS_SOURCES_MESH_MESH_H_
#define CGTECHNIQUESTEPS_SOURCES_MESH_MESH_H_

#include <string>

enum class VertexDeclarationFlags
{
	position  =   1,
	normal 	  =   2,
	color     =   4,
	texcoord  =   8,
	texcoord1 =  16,
	texcoord2 =  32,
	texcoord3 =  64,
	tangent   = 128,
	binormal  = 256,
};

class VertexDeclaration
{
	VertexDeclarationFlags m_flags;
public:

	size_t getSize() const;
	size_t getFlag() const;
};

class CMesh
{
	VertexDeclaration m_declaration;
public:
};

std::string vert_decl_flags_to_string(const int flags);

#endif /* CGTECHNIQUESTEPS_SOURCES_MESH_MESH_H_ */

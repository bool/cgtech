/*
 * mesh_format_obj.cpp
 *
 *  Created on: 17.09.2015
 *      Author: vasiliy
 */

#include "mesh_loader.h"
#include "../utils/string_utils.h"
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

struct mesh_face_data
{
	int index_v;  //индекс вершины
	int index_vn; //индекс нормали
	int index_vt; //индекс текстурных координат
};

struct mesh_object_t
{
	std::string name;
	std::vector<glm::vec3> channel_v;
	std::vector<glm::vec3> channel_vn;
	std::vector<glm::vec2> channel_vt;
	std::vector<mesh_face_data> faces;
};

ostream& operator << (ostream& os, const mesh_object_t& object)
{
	os << "name: '" << object.name << "'. vertices = '" << object.channel_v.size() << "', normals = '";
	os << object.channel_vn.size() << "'. tex coordinates = '" << object.channel_vt.size() << "'" << endl;
	os << "mesh indices count = " << object.faces.size();
	return os;
}

void parse_channel_vertex(mesh_object_t* object, const std::vector<std::string>& tokens)
{
	if (tokens.size() < 4) {
		cout << "can't read position from 4 tokens. must bee format 'v [x y z]'" << endl;
		return;
	}

	glm::vec3 vec;
	try {
		for (size_t i = 0; i < tokens.size() - 1; ++i) {
			stringstream(tokens[i + 1]) >> vec[i];
		}
	} catch (const std::exception& e) {
		cout << "error parse data" << endl;
	}

	object->channel_v.push_back(vec);
}

void parse_channel_normal(mesh_object_t* object, const std::vector<std::string>& tokens)
{
	if (tokens.size() < 4) {
		cout << "can't read normal from 4 tokens. must bee format 'vn [x y z]'" << endl;
		return;
	}

	glm::vec3 vec;
	try {
		for (size_t i = 0; i < tokens.size() - 1; ++i) {
			stringstream(tokens[i + 1]) >> vec[i];
		}
	} catch (const std::exception& e) {
		cout << "error parse data" << endl;
	}

	object->channel_vn.push_back(vec);
}

void parse_channel_uv_map(mesh_object_t* object, const std::vector<std::string>& tokens)
{
	if (tokens.size() < 3) {
		cout << "can't read texcoord from 4 tokens. must bee format 'v [x y z]'" << endl;
		return;
	}

	glm::vec2 vec;
	try {
		for (size_t i = 0; i < tokens.size() - 1; ++i) {
			stringstream(tokens[i + 1]) >> vec[i];
		}
	} catch (const std::exception& e) {
		cout << "error parse data" << endl;
	}

	object->channel_vt.push_back(vec);
}

void parse_channel_triangles(mesh_object_t* object, const std::vector<std::string>& tokens)
{
	if (tokens.size() < 4) {
		cout << "error parse mesh faces. must bee minimum 3 index for face" << endl;
		return;
	}

	for (size_t i = 1; i < tokens.size(); ++i) {
		auto words = string_utils::string_split(tokens[i], '/', false);
		mesh_face_data mf_data = {0};

		if (words.size() > 0 && !words[0].empty()) {
			stringstream(words[0]) >> mf_data.index_v;
		}
		if (words.size() > 1 && !words[1].empty()) {
			stringstream(words[1]) >> mf_data.index_vt;
		}
		if (words.size() > 2 && !words[2].empty()) {
			stringstream(words[2]) >> mf_data.index_vn;
		}
		object->faces.push_back(mf_data);
	}

}

bool mesh_load_obj(const char *fileName, mesh_desc_t* desc)
{
	ifstream in;
	in.open(fileName, ios::binary);
	if (!in.is_open()) {
		cout << "can't open file '" << fileName << "'" << endl;
		return false;
	}

	std::vector<mesh_object_t*> objects;
	mesh_object_t *start_object = new mesh_object_t;
	objects.push_back(start_object);
	start_object->name = "undefined";
	mesh_object_t *last = start_object;

	string line;
	while (getline(in, line)) {
		line = line.substr(0, line.find_first_of('#'));
		line = string_utils::string_trim_start(line, ' ');
		if (line.empty()) continue;

		std::vector<std::string> tokens = string_utils::string_split(line, ' ');
		std::string type(tokens[0]);

		//если читаем объект, смотрим, это первый объект в файле или нет, если нет - создаем новый,
		//а если первый - меняем ему имя с 'undefined' на заданное
		//так же, если не первый, добавляем в список нодов
		if (type == "g") {
			if (start_object != last) {
				last = new mesh_object_t;
				objects.push_back(last);
			}
			last->name = tokens[1];
		} else if (type == "v") {
			parse_channel_vertex(last, tokens);
		} else if (type == "vt") {
			parse_channel_uv_map(last, tokens);
		} else if (type == "vn") {
			parse_channel_normal(last, tokens);
		} else if (type == "f") {
			parse_channel_triangles(last, tokens);
		}
	}

	desc->flags = (VertexDeclarationFlags)((int)desc->flags | (int)VertexDeclarationFlags::position);

	for (auto obj: objects) {
		mesh_node_t* node = new mesh_node_t();
		node->format_flags |= (int)VertexDeclarationFlags::position;
		node->format_flags |= obj->channel_vn.empty() ? 0 : (int)VertexDeclarationFlags::position;
		node->format_flags |= obj->channel_vt.empty() ? 0 : (int)VertexDeclarationFlags::texcoord;

		node->name = obj->name;
		desc->nodes.push_back(node);
		for (std::vector<mesh_face_data>::const_iterator it = obj->faces.begin(); it != obj->faces.end(); ++it) {
			const mesh_face_data& index_data = *it;
			mesh_vertex vertex;
			vertex.position = obj->channel_v[index_data.index_v - 1];

			//если нормали у нас есть, ищем индекс нормали
			if (!obj->channel_vn.empty()) {
				int index = ((index_data.index_vn > 0) ? index_data.index_vn : index_data.index_v) - 1;
				vertex.normal = obj->channel_vn[index];
			}

			if (!obj->channel_vt.empty()) {
				int index = ((index_data.index_vt > 0) ? index_data.index_vt : index_data.index_v) - 1;
				vertex.texcoord = obj->channel_vt[index];
			}

			node->vertices.push_back(vertex);
		}

		delete obj;
	}

	cout << "load object. nodes count = '" << desc->nodes.size() << "'" << endl;
	for (const mesh_node_t* node: desc->nodes) {
		cout << " -- node '" << node->name << "':" << endl;
		cout << " ---- vertices = " << node->vertices.size() << endl;
		cout << " ---- format = " << vert_decl_flags_to_string(node->format_flags) << endl;
	}
	cout << "end." << endl;

	return true;
}

void mesh_free_obj(mesh_desc_t *mesh)
{
	std::vector<mesh_node_t*>::iterator it = mesh->nodes.begin();
	for (; it != mesh->nodes.end(); ++it) {
		delete *it;
	}
}

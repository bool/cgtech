/*
 * mesh_loader.h
 *
 *  Created on: 17.09.2015
 *      Author: vasiliy
 */

#ifndef MESH_LOADER_H_
#define MESH_LOADER_H_

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <vector>
#include <string>
#include <iostream>
#include "mesh.h"

//position, normal, texcoord
struct mesh_vertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 texcoord;
};

struct mesh_node_t
{
	int format_flags;
	std::string name;
	std::vector<mesh_vertex> vertices;
	std::vector<unsigned int> indices;
};

struct mesh_desc_t
{
	VertexDeclarationFlags flags;
	std::vector<mesh_node_t*> nodes;
};

bool mesh_load_obj(const char *fileName, mesh_desc_t* desc);
void mesh_free_obj(mesh_desc_t *mesh);

inline std::ostream& operator << (std::ostream& os, const mesh_node_t& node)
{
	os << "node name: '" << node.name;
	os << "', vertex count = '" << node.vertices.size();
	os << "', index count = '" << node.indices.size();
	os << "'";
	return os;
}

inline std::ostream& operator << (std::ostream& os, const mesh_vertex& vertex)
{
	os << "vertex = {";
	os << glm::to_string(vertex.position);
	os << ", " << glm::to_string(vertex.normal);
	os << ", " << glm::to_string(vertex.texcoord);
	os << "}";
	return os;
}

#endif /* MESH_LOADER_H_ */

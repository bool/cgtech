/*
 * mesh.cpp
 *
 *  Created on: 05.11.2015
 *      Author: vasiliy
 */

#include "mesh.h"
#include <sstream>

std::string vert_decl_flags_to_string(const int flags)
{
	std::stringstream ss;

	if (flags & (int)VertexDeclarationFlags::position) {
		ss << "position";
	}

	if (flags & (int)VertexDeclarationFlags::normal) {
		ss << " | " << "normal";
	}

	if (flags & (int)VertexDeclarationFlags::color) {
		ss << " | " << "color";
	}

	if (flags & (int)VertexDeclarationFlags::texcoord) {
		ss << " | " << "texcoord0";
	}

	if (flags & (int)VertexDeclarationFlags::texcoord1) {
		ss << " | " << "texcoord1";
	}

	if (flags & (int)VertexDeclarationFlags::texcoord2) {
		ss << " | " << "texcoord2";
	}

	if (flags & (int)VertexDeclarationFlags::texcoord3) {
		ss << " | " << "texcoord3";
	}

	if (flags & (int)VertexDeclarationFlags::tangent) {
		ss << " | " << "tangent";
	}

	if (flags & (int)VertexDeclarationFlags::binormal) {
		ss << " | " << "binormal";
	}

	return ss.str();
}

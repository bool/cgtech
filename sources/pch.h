/*
 * pch.h
 *
 *  Created on: 10.11.2015
 *      Author: vasiliy
 */

#ifndef PCH_H_
#define PCH_H_

//STL headers
#include <list>
#include <string>
#include <vector>
#include <algorithm>

//mathematics
#include <glm/glm.hpp>
#include <glm/ext.hpp>

typedef std::string String;

#endif /* PCH_H_ */

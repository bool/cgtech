/*
 * main.cpp
 *
 *  Created on: 15.09.2015
 *      Author: vasiliy
 */

//lol
#include <GL/glew.h>

#include <cstdlib>
#include <GLFW/glfw3.h>
#include <GL/glu.h>
#include <iostream>

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include "utils/string_utils.h"
#include "mesh/mesh_loader.h"
#include "shaders/shader_program.h"
#include "scene/transform.h"

using namespace std;
using namespace glm;

static vec3 mouse_position;
static vec3 mouse_delta;

vec3 camera_position(3, 4, 4);
vec2 camera_rotation(5.0f, 25.0f);

mat4 camera_view_matrix;
mat4 camera_proj_matrix;

GLuint vbo;

class RenderObject: public Transform
{
public:
	GLuint vbo;
	GLuint count;

	//logic
	bool animate;
	vec3 angularVelocity;

	//material settings
	vec4 color;
	bool shadowCast;
	bool shadowReceive;

	RenderObject(GLuint vbo, GLuint count) :
			vbo(vbo), count(count)
	{
		color = vec4(1,1,1,1);
		shadowCast = true;
		shadowReceive = false;
		animate = false;
	}

	void update(const float deltaTime)
	{
		if (animate)
		{
			m_rotation *= quat(angularVelocity*deltaTime);
		}
	}

	void draw(CShaderProgram* shader, const glm::mat4& viewproj)
	{
		mat4 model = getMatrix();
		shader->setUniformMatrix("matrixModelViewProjection", viewproj * model);
		shader->setUniformMatrix("matrixModel", model);
		shader->setUniformVector("color", color);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		int attr_vertex = glGetAttribLocation(shader->shader_program_id, "vertex");
		if (attr_vertex != -1)
		{
			glVertexAttribPointer(attr_vertex, 3, GL_FLOAT, GL_FALSE,
					sizeof(mesh_vertex), BUFFER_OFFSET(0));
			glEnableVertexAttribArray(attr_vertex);
		}
		int attr_normal = glGetAttribLocation(shader->shader_program_id,
				"normal");
		if (attr_normal != -1)
		{
			glVertexAttribPointer(attr_normal, 3, GL_FLOAT, GL_TRUE,
					sizeof(mesh_vertex), BUFFER_OFFSET(12));
			glEnableVertexAttribArray(attr_normal);
		}
		int attr_texcoord = glGetAttribLocation(shader->shader_program_id,
				"texcoord");
		if (attr_texcoord != -1)
		{
			glVertexAttribPointer(attr_vertex, 2, GL_FLOAT, GL_FALSE,
					sizeof(mesh_vertex), BUFFER_OFFSET(24));
			glEnableVertexAttribArray(attr_texcoord);
		}
		glDrawArrays(GL_TRIANGLES, 0, count);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		if (attr_vertex != -1)
		{
			glDisableVertexAttribArray(attr_vertex);
		}

		if (attr_normal != -1)
		{
			glDisableVertexAttribArray(attr_normal);
		}

		if (attr_texcoord != -1)
		{
			glDisableVertexAttribArray(attr_texcoord);
		}
	}
};

struct light_t
{
	glm::vec3 direction;
	glm::vec4 color;
};

std::vector<light_t> lights;
std::vector<RenderObject*> sceneObject_all;

CShaderProgram *diffuse_shader;

static bool wireframeRendering = false;

RenderObject* addSceneObject(const int count, const unsigned int vbo,
		const vec3& pos)
{
	RenderObject* ro = new RenderObject(vbo, count);
	ro->m_position = pos;
	sceneObject_all.push_back(ro);
	return ro;
}

static bool app_init_scene()
{
	//setup camera
	camera_view_matrix = lookAt(vec3(0, 4, -10), vec3(0, 0, 0), vec3(0, 1, 0));
	camera_proj_matrix = glm::perspective(glm::radians(60.0f), 1024.0f / 768.0f,
			0.1f, 100.0f);

	diffuse_shader = new CShaderProgram();
	if (!diffuse_shader->load("./assets/shaders/diffuse.glsl"))
	{
		return false;
	}

	//load mesh
	mesh_desc_t* desc = new mesh_desc_t();
	if (!mesh_load_obj("./assets/mesh/cube.obj", desc))
	{
		return false;
	}

	//make cube vbo
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER,
			sizeof(mesh_vertex) * desc->nodes[0]->vertices.size(),
			&(desc->nodes[0]->vertices[0]), GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Setup GL
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);

	//make custom scene
	glClearColor(0.0, 0.0, 0.0, 1.0);

	auto vertexCount = desc->nodes[0]->vertices.size();
	mesh_free_obj(desc);

	RenderObject* root = addSceneObject(vertexCount, vbo, vec3(0, 0, 0));
	root->m_scale = vec3(10.0f, 0.1f, 10.0f);

	RenderObject* rotatedBox1 = addSceneObject(vertexCount, vbo, vec3(0, 3, 0));
	rotatedBox1->angularVelocity = vec3(0.36f, 0.0f, 0.0f);
	rotatedBox1->animate = true;
	rotatedBox1->color = vec4(1,0,0,1);

	RenderObject* rotatedBox2 = addSceneObject(vertexCount, vbo, vec3(3, 0, 0));
	rotatedBox2->angularVelocity = vec3(0.0f, 0.72f, 0.0f);
	rotatedBox2->animate = true;
	rotatedBox2->color = vec4(0,1,0,0.25);
	rotatedBox1->attach(rotatedBox2);

	RenderObject* rotatedBox3 = addSceneObject(vertexCount, vbo, vec3(0, 0, 3));
	rotatedBox3->angularVelocity = vec3(0.0f, 0.0f, 0.12f);
	rotatedBox3->animate = true;
	rotatedBox3->color = vec4(0,0,1,0.5);
	rotatedBox3->attach(rotatedBox1);

	//make lights
	lights.push_back(light_t { vec3(1,1,0), vec4(0.8,0.8,0.8,1) });

	return true;
}

void drawLightedScene(CShaderProgram* shader, const glm::mat4& viewproj)
{
	//draw in forward rendering with additive pass for any light
	shader->bind();
	for (const auto light: lights) {
//		glEnable(GL_BLEND);
//		glBlendFunc(GL_ONE, GL_ONE);

		shader->setUniformVector("lightDir", light.direction);
		shader->setUniformVector("lightCol", light.color);
		for (const auto obj : sceneObject_all) {
			obj->draw(shader, viewproj);
		}
	}
}

static void app_draw_frame(int width, int height, const double deltaTime)
{
	glPolygonMode(GL_FRONT_AND_BACK, wireframeRendering ? GL_LINE : GL_FILL);

	glViewport(0, 0, width, height);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDisable(GL_CULL_FACE);

	// ==================================================================
	//	simple rotations logics
	for (const auto obj : sceneObject_all)
	{
		obj->update((float)deltaTime);
	}
	// ==================================================================

	//draw to shadowmap

	//draw from one pass for light
	mat4 cameraViewProj = camera_proj_matrix * camera_view_matrix;
	drawLightedScene(diffuse_shader, cameraViewProj);

	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		cout << "GL error code = '" << error << "'. string = '"
				<< gluErrorString(error) << "'" << endl;
	}
}

static void app_free_scene()
{
	delete diffuse_shader;
}

/*
 * урпавление камерой
 */
static void input_update(GLFWwindow* window, const double delta)
{
	vec3 old_pos = mouse_position;

	double new_pos_x, new_pos_y;
	glfwGetCursorPos(window, &new_pos_x, &new_pos_y);
	mouse_position = vec3((float) new_pos_x, (float) new_pos_y,
			mouse_position.z);
	mouse_delta = mouse_position - old_pos;

	if (glfwGetMouseButton(window, 0))
	{
		camera_rotation += vec2(-mouse_delta.x, mouse_delta.y);
	}

	vec3 up(0, 1, 0);
	mat4 rot_y = rotate(radians(camera_rotation.x), up);
	vec3 fv = vec3(rot_y * vec4(0, 0, 1, 0));
	mat4 rot_x = rotate(radians(camera_rotation.y), cross(fv, up));
	mat4 camera_basis = rot_x * rot_y;

	vec3 dir = vec3(camera_basis * vec4(0, 0, 1, 0));
	vec3 side = vec3(camera_basis * vec4(1, 0, 0, 0));

	vec3 velocity(0, 0, 0);
	if (glfwGetKey(window, GLFW_KEY_W))
		velocity -= dir;
	if (glfwGetKey(window, GLFW_KEY_A))
		velocity -= side;
	if (glfwGetKey(window, GLFW_KEY_S))
		velocity += dir;
	if (glfwGetKey(window, GLFW_KEY_D))
		velocity += side;
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT)
			|| glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT))
	{
		velocity *= 1.7f;
	}

	camera_position += velocity * delta;
	camera_view_matrix = inverse(camera_basis) * translate(-camera_position);
	//cout << "camera position = " << to_string(camera_position) << ". camera rot = " << to_string(camera_rotation) << endl;
}

static void window_focus_callback(GLFWwindow* window, int focus)
{
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action,
		int mods)
{
	if (action == GLFW_PRESS)
	{
		switch (key)
		{
		case GLFW_KEY_ESCAPE:
			glfwSetWindowShouldClose(window, GL_TRUE);
			break;

		case GLFW_KEY_F:
			wireframeRendering = !wireframeRendering;
			break;
		}
	}
}

static void error_callback(int error, const char *description)
{
	cout << description << endl;
}

int main(int argc, char *argv[])
{
	cout << "working directory: " << argv[0] << endl;

	if (!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	glfwSetErrorCallback(error_callback);

	glfwWindowHint(GLFW_DEPTH_BITS, 32);
	glfwWindowHint(GLFW_RESIZABLE, 0);

	GLFWwindow* window = glfwCreateWindow(1024, 768, "OpenGL window", NULL,
			NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwSetKeyCallback(window, key_callback);
	glfwSetWindowFocusCallback(window, window_focus_callback);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);

	if (GLEW_OK != glewInit())
	{
		cout << "can't init gl extension wrangler" << endl;
		return -1;
	}

	cout << "GL vendor:    " << glGetString(GL_VENDOR) << endl;
	cout << "GL renderer:  " << glGetString(GL_RENDERER) << endl;
	cout << "GL version:   " << glGetString(GL_VERSION) << endl;
	cout << "GLEW version: " << glewGetString(GLEW_VERSION) << endl;
	cout << "GLFW version: " << glfwGetVersionString() << endl;

	if (!app_init_scene())
	{
		cout << "application initialize failed!" << endl;
		glfwDestroyWindow(window);
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	cout << "application initialize ready!" << endl;

	double cursorx, cursory;
	glfwGetCursorPos(window, &cursorx, &cursory);
	mouse_position = vec3(cursorx, cursory, 0.0f);

	double oldTime = glfwGetTime();

	while (!glfwWindowShouldClose(window))
	{
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);

		double newTime = glfwGetTime();
		double deltaTime = newTime - oldTime;
		oldTime = newTime;
		app_draw_frame(width, height, deltaTime);

		glfwSwapBuffers(window);
		glfwPollEvents();

		input_update(window, deltaTime);
	}

	app_free_scene();
	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}

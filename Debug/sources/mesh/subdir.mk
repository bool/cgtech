################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../sources/mesh/mesh.cpp \
../sources/mesh/mesh_loader.cpp 

OBJS += \
./sources/mesh/mesh.o \
./sources/mesh/mesh_loader.o 

CPP_DEPS += \
./sources/mesh/mesh.d \
./sources/mesh/mesh_loader.d 


# Each subdirectory must supply rules for building sources it contributes
sources/mesh/%.o: ../sources/mesh/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -std=c++11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



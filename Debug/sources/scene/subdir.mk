################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../sources/scene/sceneTree.cpp \
../sources/scene/transform.cpp 

OBJS += \
./sources/scene/sceneTree.o \
./sources/scene/transform.o 

CPP_DEPS += \
./sources/scene/sceneTree.d \
./sources/scene/transform.d 


# Each subdirectory must supply rules for building sources it contributes
sources/scene/%.o: ../sources/scene/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -std=c++11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


